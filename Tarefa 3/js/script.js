$(document).ready(function() {
    $(".botaoPost").on('click', function() {
        $.post("https://jquery-19-1.herokuapp.com/destinations", {
            destination: { name: "Quente", description: "Muito Quente", more_info: "Muito Muito Quente", price: "199"}
        }, function(msg) {
            alert(msg);
        })
    });

    $(".botaoGet").on('click', function() {
        $.get("https://jquery-19-1.herokuapp.com/destinations", function(resultado) {
            $.each(resultado, function(key, value) {
                $('#lista').append('<li>Name: '+value.name+'</li>');
                $('#lista').append('<li>Description: '+value.description+'</li>');
                $('#lista').append('<li>More Info: '+value.more_info+'</li>');
                $('#lista').append('<li>Price: '+value.price+'</li>');
                $('#lista').append('<br>');
            });
        });
    });

});