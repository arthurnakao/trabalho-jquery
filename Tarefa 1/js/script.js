$(document).ready(function() {
    $('#phone').mask('(00)00000-0000');
    $('#rg').mask('00.000.000-#');
    $('#cpf').mask('000.000.000-00');
    $('#salary').mask('#.##0,00', {reverse: true});
});